use {
    serde_derive::{Serialize, Deserialize},
    simply_frontend::{Component, GlobalEvents},
};

#[derive(Serialize, Deserialize)]
pub struct Model {
    hello: String,
    count: i32,
    add_amount: String,
    add_error: bool,
}

#[derive(Deserialize, Clone)]
pub enum Event {
    Clicked(i32),
    AddClicked,
}

pub struct CounterComponent;

impl Component for CounterComponent {
    type Model = Model;
    type Event = Event;
    
    fn template(&self) -> &'static str {
        include_str!("./counter.rht")
    }

    fn initialize(&self, _event_sink: &mut Option<&'static str>) -> Model {
        Model {
            hello: "Counter".into(),
            count: 0,
            add_amount: "".into(),
            add_error: false,
        }
    }

    fn handle_event(&self, event: Event, model: &mut Model, _global_events: &mut GlobalEvents) {
        match event {
            Event::Clicked(v) => {
                model.count += v;
            },
            Event::AddClicked => {
                let result: Result<i32, _> = model.add_amount.parse();
                match result {
                    Ok(value) => {
                        model.add_error = false;
                        model.count += value;
                    },
                    Err(_) => model.add_error = true,
                }
            },
        }

        if model.count == 42 {
            model.hello = "The Meaning of Life".into();
        } else {
            model.hello = "Counter".into();
        }
    }
}
