use {
    serde_derive::{Serialize, Deserialize},
    simply_frontend::{Component},
};

#[derive(Serialize, Deserialize)]
pub struct Model {
}

#[derive(Deserialize, Clone)]
pub enum Event {
}

pub struct HomeComponent;

impl Component for HomeComponent {
    type Model = Model;
    type Event = Event;
    
    fn template(&self) -> &'static str {
        include_str!("./home.rht")
    }

    fn initialize(&self, _event_sink: &mut Option<&'static str>) -> Model {
        Model {
        }
    }
}
