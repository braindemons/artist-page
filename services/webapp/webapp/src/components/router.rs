use {
    log::{info},
    serde_derive::{Serialize, Deserialize},
    simply_frontend::{Component, GlobalEvents},
};

#[derive(Serialize, Deserialize)]
pub struct Model {
    active_component: String,
}

#[derive(Deserialize, Clone)]
pub enum RouterEvent {
    Navigate(String),
}

pub struct RouterComponent;

impl Component for RouterComponent {
    type Model = Model;
    type Event = RouterEvent;
    
    fn template(&self) -> &'static str {
        include_str!("./router.rht")
    }

    fn initialize(&self, event_sink: &mut Option<&'static str>) -> Model {
        *event_sink = Some("router");

        Model {
            active_component: "route-home".into(),
        }
    }

    fn handle_event(
        &self, event: RouterEvent, model: &mut Model, _global_events: &mut GlobalEvents,
    ) {
        match event {
            RouterEvent::Navigate(route) => {
                info!("Navigating to \"{}\"", route);
                model.active_component = route;
            },
        }
    }
}
