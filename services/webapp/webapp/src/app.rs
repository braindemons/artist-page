use {
    serde_derive::{Serialize, Deserialize},
    simply_frontend::{Component, GlobalEvents},
    crate::components::router::{RouterEvent},
};

#[derive(Serialize, Deserialize)]
pub struct Model {
    navbar_active: bool,
}

#[derive(Deserialize, Clone)]
pub enum Event {
    NavbarToggled,
    NavClicked(String),
}

pub struct AppComponent;

impl Component for AppComponent {
    type Model = Model;
    type Event = Event;
    
    fn template(&self) -> &'static str {
        include_str!("./app.rht")
    }

    fn initialize(&self, _event_sink: &mut Option<&'static str>) -> Model {
        Model {
            navbar_active: false,
        }
    }

    fn handle_event(&self, event: Event, model: &mut Model, global_events: &mut GlobalEvents) {
        match event {
            Event::NavbarToggled =>
                model.navbar_active = !model.navbar_active,
            Event::NavClicked(route) =>
                global_events.queue("router", RouterEvent::Navigate(route)),
        }
    }
}
