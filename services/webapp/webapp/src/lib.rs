mod components;
mod routes;
mod app;

use {
    serde_derive::{Serialize, Deserialize},
    simply_frontend::{App},
};

pub fn app() -> Result<App, String> {
    let mut app = App::new("app");

    app.add_class("app", app::AppComponent)?;
    app.add_class("router", components::router::RouterComponent)?;
    app.add_class("route-counter", routes::counter::CounterComponent)?;
    app.add_class("route-home", routes::home::HomeComponent)?;

    Ok(app)
}
