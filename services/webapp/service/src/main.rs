use {
    log::{error},
    simply_service::{
        handlers::{self, MemoryDirectoryHandler},
        request::{Response, Request, Result},
        mime,
        Server, Method,
    },
    simply_frontend::{App},
};

struct Shared {
    app: App,
}

#[allow(clippy::needless_pass_by_value)]
fn render_html(shared: &Shared, _request: Request) -> Result {
    let template = include_str!("../index.html");

    let app_html = simply_frontend_server::render_html(&shared.app)?;
    let html = template.replace("$mounting_point", &app_html);

    Ok(Response {
        body: html.into(),
        content_type: Some(mime::TEXT_HTML),
    })
}

fn main() {
    simply_service::setup_logging();
    
    let app = match artist_page_webapp::app() {
        Ok(app) => app,
        Err(error) => {
            error!("Failed to initialize app:\n{}", error);
            return
        }
    };

    let shared = Shared {
        app,
    };

    let mut server = Server::build();
    server.add_route(
        "/static/*",
        Method::GET,
        MemoryDirectoryHandler::new("../client/dist/"),
    );
    server.add_route("/favicon.ico", Method::GET, handlers::not_found);
    server.add_route("/*", Method::GET, render_html);
    server.run(&"0.0.0.0:8000".parse().unwrap(), shared);
}
