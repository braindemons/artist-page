use {
    log::{error},
    wasm_bindgen::prelude::*,
    simply_frontend_client::{mount},

    artist_page_webapp::{app},
};

#[wasm_bindgen]
pub fn initialize() {
    console_error_panic_hook::set_once();
    wasm_logger::init().unwrap();
    
    let result = app().and_then(mount);

    if let Err(error) = result {
        error!("Error Initializing: {}", error);
    }
}