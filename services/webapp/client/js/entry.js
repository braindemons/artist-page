const rust = import('../.wasm/artist_page_webapp_client');

rust.then(m => {
    // If the DOM's already loaded, load now, if not, defer till the DOM's loaded
    if (document.readyState === "complete") {
        m.initialize();
    } else {
        document.addEventListener("DOMContentLoaded", () => {
            m.initialize();
        });
    }
});