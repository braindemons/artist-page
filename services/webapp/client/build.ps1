# Build the crate that exposes the initialize WASM function
cargo build --manifest-path ./crate/Cargo.toml --target wasm32-unknown-unknown --release

# Remove the existing wasm-bindgen artifacts directory
md ./.wasm -ea 0 *>$null

# Generate the bindings for the crate
wasm-bindgen ../../../target/wasm32-unknown-unknown/release/artist_page_webapp_client.wasm --out-dir ./.wasm

# Finally, run webpack to bundle it all together
npx webpack