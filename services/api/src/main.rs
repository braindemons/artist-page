use {
    std::sync::atomic::{AtomicUsize, Ordering},

    log::{info},
    serde_derive::{Serialize, Deserialize},
    simply_service::{request::{Request, Response, Result}, Server, Method},
};

#[derive(Serialize, Deserialize)]
struct HelloCountResponse {
    count: usize,
}

#[allow(clippy::needless_pass_by_value)]
fn hello_count(shared: &Shared, _request: Request) -> Result {
    let count = shared.hello_count.load(Ordering::Relaxed);
    info!("Reporting hello count {}", count);

    let response = HelloCountResponse {
        count,
    };
    Ok(Response::serialize_body(response)?)
}

#[derive(Serialize, Deserialize)]
struct HelloRequest {
    name: String,
}

#[derive(Serialize, Deserialize)]
struct HelloResponse {
    message: String,
}

#[allow(clippy::needless_pass_by_value)]
fn hello(shared: &Shared, request: Request) -> Result {
    let request: HelloRequest = request.deserialize_body()?;

    info!("Saying hello to {}", request.name);
    shared.hello_count.fetch_add(1, Ordering::Relaxed);

    let response = HelloResponse {
        message: format!("Hello, {}!", request.name)
    };
    Ok(Response::serialize_body(response)?)
}

struct Shared {
    hello_count: AtomicUsize,
}

fn main() {
    simply_service::setup_logging();

    let shared = Shared {
        hello_count: Default::default(),
    };

    let mut server = Server::build();
    server.add_route("/hello", Method::GET, hello_count);
    server.add_route("/hello", Method::POST, hello);
    server.run(&"0.0.0.0:8001".parse().unwrap(), shared);
}