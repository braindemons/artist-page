# Artist Page

## Build & Run
`--build` makes sure any already existing images are re-built.
```bash
docker-compose up --build
```

## Shut Down
```bash
docker-compose down
```
Additional options:
- `--rmi local` removes the images.
- `--volumes` remove the volumes.
```bash
docker-compose down --rmi local --volumes
```

## Cleanup
If you've been working for a while and have dangling containers and images laying around, use the
following command to clean up the images.
```bash
docker image prune
```
Or the following to clean up *everything*.
```bash
docker system prune
```

## Frontend Setup
```bash
rustup target add wasm32-unknown-unknown
cargo install wasm-bindgen-cli --bin wasm-bindgen
./build.ps1
```

Building in `--release` mode is currently necessary, as debug `wasm32-unknown-unknown` is broken.

## License
Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
