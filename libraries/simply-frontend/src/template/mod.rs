mod compile;

pub use {
    self::compile::{compile},
};

use {
    serde_json::{Value, Map},
};

#[derive(Debug)]
pub struct Element {
    pub name: ElementName,
    pub attributes: Attributes,
    /// If None this is a void element, non-void elements are Some with empty vector.
    pub nodes: Option<Vec<Node>>,
}

#[derive(Debug)]
pub enum ElementName {
    Tag(String),
    Component(String),
    ComponentBinding(ValueBinding),
}

#[derive(Debug)]
pub enum Node {
    Element(Element),
    Binding(ValueBinding),
    Text {
        text: String,
        escape: bool,
    },
}

#[derive(Debug)]
pub struct Attributes {
    /// The value is only used once when the component is rendered.
    pub one_time: Vec<(String, String)>,
    /// Send data from the view to the component.
    pub from_view: Vec<(String, ValueBinding)>,
    /// Dynamic classes that are turned on or off by model bindings.
    pub to_view_classes: Vec<(String, ValueBinding)>,
    /// Events that should be raised in the component.
    pub events: Vec<(String, Event)>,
}

#[derive(Clone, Debug)]
pub struct Event(pub String);

#[derive(Clone, Debug)]
pub struct ValueBinding(pub String);

impl ValueBinding {
    pub fn resolve_string(&self, model: &Map<String, Value>) -> Result<String, String> {
        let value = model.get(&self.0)
            .ok_or_else(|| format!("Unknown binding identifier {}", self.0))?;

        Ok(value_to_string(value))
    }

    pub fn resolve_bool(&self, model: &Map<String, Value>) -> Result<bool, String> {
        let value = model.get(&self.0)
            .ok_or_else(|| format!("Unknown binding identifier {}", self.0))?;

        Ok(value_to_bool(value))   
    }

    pub fn resolve_replace_string(
        &self, model: &mut Map<String, Value>, new_value: String
    ) -> Result<(), String> {
        let value = model.get_mut(&self.0)
            .ok_or_else(|| format!("Unknown binding identifier {}", self.0))?;

        if !value.is_string() {
            return Err("Value must be a string for it to be a target for input bindings".to_string())
        }

        *value = Value::String(new_value);

        Ok(())
    }
}

pub fn value_to_string(value: &Value) -> String {
    match value {
        Value::String(text) => text.clone(),
        other => other.to_string(),
    }
}

pub fn value_to_bool(value: &Value) -> bool {
    match value {
        Value::Bool(value) => *value,
        _ => true,
    }
}