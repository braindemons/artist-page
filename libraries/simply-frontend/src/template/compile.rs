use {
    pest::{iterators::{Pair}, Parser},

    rhinestone_pest::{RhinestoneParser, Rule},

    crate::{
        template::{Element, Node, Attributes, ValueBinding, Event, ElementName},
    },
};

pub fn compile(template: &str) -> Result<Element, String> {
    let mut pairs = RhinestoneParser::parse(Rule::template, template)
        .map_err(|e| format!("{}", e))?;

    let pair = pairs.next().unwrap();
    Ok(parse_element(pair)?)
}

fn parse_element(pair: Pair<Rule>) -> Result<Element, String> {
    assert_eq!(pair.as_rule(), Rule::element);

    let mut name = None;
    let mut attributes = None;
    let mut nodes = None;

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::tag => {
                let tag_str = pair.into_span().as_str().to_string();

                // Check if this is a void element, if it isn't we should allocate a nodes vector
                match tag_str.as_str() {
                    "area" | "base" | "br" | "col" | "embed" | "hr" | "img" | "input" | "link" |
                    "meta" | "param" | "source" | "track" | "wbr" => {},
                    _ => nodes = Some(Vec::new())
                }

                name = Some(ElementName::Tag(tag_str))
            },
            Rule::component => {
                let pair = pair.into_inner().next().unwrap();
                match pair.as_rule() {
                    Rule::identifier => {
                        let component_name = pair.into_span().as_str().to_string();
                        name = Some(ElementName::Component(component_name));
                    },
                    Rule::value_binding => {
                        let ident = pair.into_inner().next().unwrap();
                        assert_eq!(ident.as_rule(), Rule::identifier);
                        let binding = ident.into_span().as_str().to_string();
                        name = Some(ElementName::ComponentBinding(ValueBinding(binding)));
                    },
                    _ => unreachable!(),
                }
            },
            Rule::attributes =>
                attributes = Some(parse_attributes(pair)?),
            Rule::nodes => {
                if let Some(nodes) = &mut nodes {
                    parse_content(pair, nodes)?
                } else {
                    return Err("Void elements are not allowed to have content nodes".into())
                }
            },
            _ => unreachable!()
        }
    }

    Ok(Element {
        name: name.unwrap(),
        attributes: attributes.unwrap(),
        nodes,
    })
}

fn parse_attributes(pair: Pair<Rule>) -> Result<Attributes, String> {
    let mut attributes = Attributes {
        one_time: Vec::new(),
        from_view: Vec::new(),
        to_view_classes: Vec::new(),
        events: Vec::new(),
    };
    let mut classes = Vec::new();

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::id => {
                let id = pair.into_span().as_str()[1..].to_string();
                attributes.one_time.push(("id".into(), id));
            },
            Rule::class => {
                let mut class = None;
                let mut binding = None;

                for pair in pair.into_inner() {
                    match pair.as_rule() {
                        Rule::class_identifier =>
                            class = Some(pair.into_span().as_str()[1..].to_string()),
                        Rule::value_binding =>
                            binding = Some(parse_value_binding(pair)),
                        _ => unreachable!()
                    }
                }

                if binding.is_none() {
                    classes.push(class.unwrap());
                } else {
                    attributes.to_view_classes.push((class.unwrap(), binding.unwrap()));
                }
            },
            Rule::attribute => {
                parse_attribute(pair, &mut attributes)?;
            },
            _ => unreachable!()
        }
    }

    // Combine classes into a single attribute
    if !classes.is_empty() {
        let mut class_str = String::new();

        for (i, class) in classes.iter().enumerate() {
            class_str.push_str(class);

            // Last one does not need a trailing space
            if i != classes.len()-1 {
                class_str.push(' ');
            }
        }

        attributes.one_time.push(("class".into(), class_str));
    }

    Ok(attributes)
}

fn parse_attribute(pair: Pair<Rule>, attributes: &mut Attributes) -> Result<(), String> {
    let mut modifier = None;
    let mut key = None;
    let mut value = None;
    let mut binding = None;

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::attribute_modifier => {
                modifier = Some(pair.into_span().as_str().to_string());
            }
            Rule::attribute_key => {
                key = Some(pair.into_span().as_str().to_string());
            }
            Rule::attribute_value => {
                let pair = pair.into_inner().next().unwrap();
                match pair.as_rule() {
                    Rule::string_literal => {
                        let value_str = pair.into_span().as_str();
                        assert!(value_str.len() >= 2);
                        value = Some(value_str[1..value_str.len()-1].to_string());
                    },
                    Rule::value_binding => {
                        binding = Some(parse_value_binding(pair));
                    },
                    _ => unreachable!()
                }
            },
            _ => unreachable!()
        }
    }

    if let Some(modifier) = modifier {
        match modifier.as_str() {
            "on" => {
                let value = value
                    .ok_or_else(|| "Event attributes must have a string literal".to_string())?;
                attributes.events.push((key.unwrap(), Event(value)));
            },
            "from-view" => {
                let binding = binding
                    .ok_or_else(|| "From-view attributes must have a binding".to_string())?;
                attributes.from_view.push((key.unwrap(), binding));
            },
            modifier =>
                return Err(format!("Modifier \"{}\" is not supported", modifier)),
        }
    } else {
        let value = value
            .ok_or_else(|| "Plain attributes must have a string value".to_string())?;
        attributes.one_time.push((key.unwrap(), value));
    }

    Ok(())
}

fn parse_content(
    pair: Pair<Rule>, content: &mut Vec<Node>,
) -> Result<(), String> {
    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::element => content.push(
                Node::Element(parse_element(pair)?)
            ),
            Rule::value_binding => {
                let key = parse_value_binding(pair);
                content.push(Node::Binding(key));
            },
            Rule::text => {
                let text = pair.into_span().as_str();
                assert!(text.len() >= 2);

                if !text.starts_with('=') {
                    content.push(Node::Text{
                        text: text[1..text.len()-1].to_string(),
                        escape: true,
                    });
                } else {
                    content.push(Node::Text {
                        text: text[2..text.len()-1].to_string(),
                        escape: false,
                    });
                }
            },
            _ => unreachable!()
        }
    }

    Ok(())
}

fn parse_value_binding(pair: Pair<Rule>) -> ValueBinding {
    let ident = pair.into_inner().next().unwrap();
    assert_eq!(ident.as_rule(), Rule::identifier);
    
    ValueBinding(ident.into_span().as_str().to_string())
}