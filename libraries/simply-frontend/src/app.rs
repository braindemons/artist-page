use {
    std::{
        collections::{HashMap},
        sync::{Arc},
    },

    crate::{
        template,
        data::{ComponentData, ComponentDataConcrete},
        Component,
    },
};

pub struct App {
    root_name: &'static str,
    classes: HashMap<&'static str, ComponentClass>,
}

impl App {
    pub fn new(root_name: &'static str) -> App {
        App {
            root_name,
            classes: HashMap::new(),
        }
    }

    pub fn root_name(&self) -> &'static str {
        self.root_name
    }

    pub fn class(&self, name: &str) -> Result<&ComponentClass, String> {
        self.classes.get(name)
            .ok_or_else(|| format!("Component \"{}\" does not exist", name))
    }

    pub fn add_class<C: Component>(
        &mut self,
        name: &'static str,
        component: C,
    ) -> Result<(), String> {
        if self.classes.contains_key(name) {
            return Err(format!("Component \"{}\" already exists", name))
        }

        let template = template::compile(component.template())
            .map_err(|e| format!("Failed to parse template for \"{}\":\n{}", name, e))?;
        let component = Arc::new(component);

        let data_factory = Box::new(move || {
            let mut event_sink = None;
            
            // Run the actual component initialization
            let model = component.initialize(&mut event_sink);

            // Bundle the component data that needs to be stored in a generic way
            let data = ComponentDataConcrete {
                model,
                component: component.clone(),
            };
            
            ComponentBundle {
                event_sink,
                data: Box::new(data),
            }
        });

        self.classes.insert(name, ComponentClass {
            template,
            data_factory,
        });

        Ok(())
    }

    pub fn initialize(
        &self, name: &str,
    ) -> Result<ComponentBundle, String> {
        let root_class = self.class(name)?;
        let bundle = root_class.create_data();

        Ok(bundle)
    }
}

pub struct ComponentClass {
    pub template: template::Element,
    data_factory: Box<Fn() -> ComponentBundle + Send + Sync>,
}

impl ComponentClass {
    pub fn create_data(&self) -> ComponentBundle {
        (self.data_factory)()
    }
}

pub struct ComponentBundle {
    pub event_sink: Option<&'static str>,
    pub data: Box<ComponentData>,
}