pub mod template;

mod app;
mod data;

pub use self::{
    app::{App, ComponentBundle},
    data::{ComponentData},
};

use {
    std::{
        collections::{VecDeque},
        any::{Any},
    },

    serde::{Serialize, de::DeserializeOwned},
};

pub trait Component: Any + Send + Sync {
    type Model: Serialize + DeserializeOwned + Any;
    type Event: DeserializeOwned + Any + Clone;

    fn template(&self) -> &'static str;

    fn initialize(&self, _event_sink: &mut Option<&'static str>) -> Self::Model;

    fn handle_event(
        &self, _event: Self::Event, _model: &mut Self::Model, _global_events: &mut GlobalEvents,
    ) {}
}

pub struct GlobalEvents<'a> {
    events: &'a mut VecDeque<(&'static str, Box<Any>)>,
}

impl<'a> GlobalEvents<'a> {
    pub fn new(events: &'a mut VecDeque<(&'static str, Box<Any>)>) -> Self {
        GlobalEvents {
            events,
        }
    }

    pub fn queue<E: Any>(&mut self, sink: &'static str, event: E) {
        self.events.push_back((sink, Box::new(event)));
    }
}
