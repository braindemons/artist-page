use {
    std::{
        any::{Any},
        sync::{Arc},
    },

    serde_json::{Value, Map},

    crate::{
        template, Component, GlobalEvents,
    },
};

pub trait ComponentData {
    fn model_to_object(&self) -> Result<Map<String, Value>, String>;

    fn object_to_model(&mut self, object: Map<String, Value>) -> Result<(), String>;

    fn parse_event(&self, event: &template::Event) -> Result<Box<Any>, String>;

    fn raise_event(
        &mut self, event: &Box<Any>, global_events: &mut GlobalEvents,
    ) -> Result<(), String>;
}

pub struct ComponentDataConcrete<C: Component> {
    pub model: C::Model,
    pub component: Arc<C>,
}

impl<C: Component> ComponentData for ComponentDataConcrete<C> {
    fn model_to_object(&self) -> Result<Map<String, Value>, String> {
        let value = serde_json::value::to_value(&self.model)
            .map_err(|e| format!("Error Serializing Model: {}", e))?;

        if let Value::Object(object) = value {
            Ok(object)
        } else {
            Err("Rendering Error: model is not an object".to_string())
        }
    }

    fn object_to_model(&mut self, object: Map<String, Value>) -> Result<(), String> {
        self.model = serde_json::value::from_value(Value::Object(object))
            .map_err(|e| format!("Error Deserializing Model: {}", e))?;
        Ok(())
    }

    fn parse_event(&self, event: &template::Event) -> Result<Box<Any>, String> {
        let event: C::Event = ron::de::from_str(&event.0)
            .map_err(|e| format!("Error Parsing Event: {}", e))?;

        Ok(Box::new(event))
    }

    fn raise_event(
        &mut self, event: &Box<Any>, global_events: &mut GlobalEvents,
    ) -> Result<(), String> {
        // Convert the Any event to the type we need
        let event: &C::Event = event.downcast_ref()
            .ok_or_else(|| "Error Raising Event: Invalid event type received".to_string())?;

        self.component.handle_event(event.clone(), &mut self.model, global_events);
        Ok(())
    }
}
