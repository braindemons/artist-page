use {
    std::{
        collections::{HashMap},
        path::{Path},
        fs,
    },

    mime::{Mime},
    mime_guess::{guess_mime_type},

    crate::{
        request::{Request, Response, Error, Result},
        Handler,
    },
};

/// Reads in a file to memory, and serves it up on request. Guesses mimetype from file extension.
pub struct MemoryFileHandler {
    data: Vec<u8>,
    mime: Mime,
}

impl MemoryFileHandler {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        let data = fs::read(&path).unwrap();
        let mime = guess_mime_type(path);

        Self {
            data,
            mime,
        }
    }
}

impl<S> Handler<S> for MemoryFileHandler {
    fn handle(&self, _shared: &S, _request: Request) -> Result {
        Ok(Response {
            body: self.data.clone(),
            content_type: Some(self.mime.clone()),
        })
    }
}

/// Reads in a directory's files to memory, and serves them up on request. Guesses mimetypes from
/// file extensions.
pub struct MemoryDirectoryHandler {
    paths: HashMap<String, (Vec<u8>, Mime)>,
}

impl MemoryDirectoryHandler {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        let mut paths = HashMap::new();

        for entry in fs::read_dir(path).unwrap() {
            let entry = entry.unwrap();
            if !entry.metadata().unwrap().is_file() {
                continue
            }

            let entry_path = entry.path();
            let data = fs::read(&entry_path).unwrap();
            let mime = guess_mime_type(&entry_path);
            paths.insert(entry.file_name().into_string().unwrap(), (data, mime));
        }

        Self {
            paths,
        }
    }
}

impl<S> Handler<S> for MemoryDirectoryHandler {
    fn handle(&self, _shared: &S, request: Request) -> Result {
        match self.paths.get(request.wildcard.as_ref().unwrap()) {
            Some((data, mime)) =>
                Ok(Response {
                    body: data.clone(),
                    content_type: Some(mime.clone()),
                }),
            None =>
                Err(Error::NotFound)
        }
    }
}

/// Returns a NotFound error.
#[allow(clippy::needless_pass_by_value)]
pub fn not_found<S>(_: &S, _: Request) -> Result {
    Err(Error::NotFound)
}