use {
    std::{
        any::{Any},
        sync::{Arc},
        net::{SocketAddr},
    },

    log::{info, error, warn},
    hyper::{service::{Service}, Body, StatusCode, Chunk},
    futures::{future, Future, IntoFuture, Stream},
    yansi::{Paint},

    crate::{
        request::{self, Request, Response, Error},
        Method,
    },
};

type HyperFuture = Future<Item = hyper::Response<Body>, Error = hyper::Error> + Send;

struct Route<S> {
    path: &'static str,
    wildcard: bool,
    method: Method,

    handler: Box<Handler<S>>
}

pub trait Handler<S>: Send + Sync {
    fn handle(&self, shared: &S, request: Request) -> request::Result;
}

impl<S, F> Handler<S> for F where
    F: Fn(&S, Request) -> Result<Response, Error> + Send + Sync
{
    fn handle(&self, shared: &S, request: Request) -> request::Result {
        self(shared, request)
    }
}

pub struct Server<S> {
    routes: Vec<Route<S>>,
}

impl<S: Any + Send + Sync> Server<S> {
    pub fn build() -> Self {
        Self {
            routes: Vec::new(),
        }
    }

    pub fn add_route(
        &mut self, path: &'static str, method: Method, handler: impl Handler<S> + 'static,
    ) {
        let wildcard = path.ends_with('*');
        let path = path.trim_right_matches('*');

        self.routes.push(Route {
            path,
            wildcard,
            method,

            handler: Box::new(handler),
        });
    }

    pub fn run(self, addr: &SocketAddr, shared: S) {
        let routes = Arc::new(self.routes);
        let shared = Arc::new(shared);
        let server = hyper::Server::bind(addr)
            .serve(move || ApiService {
                routes: routes.clone(),
                shared: shared.clone(),
            })
            .map_err(|e| error!("server error: {}", e));

        info!("{}Listening on {}", Paint::masked("👂 "), Paint::white(addr).bold());
        hyper::rt::run(server);
    }
}

pub struct ApiService<S> {
    routes: Arc<Vec<Route<S>>>,
    shared: Arc<S>,
}

impl<S: Any + Send + Sync> Service for ApiService<S> {
    type ReqBody = Body;
    type ResBody = Body;
    type Error = hyper::Error;
    type Future = Box<HyperFuture>;

    fn call(&mut self, request: hyper::Request<Body>) -> Box<HyperFuture> {
        let method = request.method().clone();
        let path = request.uri().path().to_string();
        let routes = self.routes.clone();
        let shared = self.shared.clone();

        info!(
            "{}{} {}{}",
            Paint::masked("🏃 "),
            Paint::green(&method),
            Paint::blue(&path),
            Paint::white(":"),
        );

        let future = request.into_body()
            .concat2()
            .and_then(move |body| dispatch_request(&method, &path, &routes, &shared, &body));

        Box::new(future)
    }
}

fn dispatch_request<S>(
    method: &Method, path: &str,
    routes: &[Route<S>],
    shared: &S,
    body: &Chunk,
) -> Box<HyperFuture> {
    for route in routes {
        let mut matches = route.method == method && !route.wildcard && route.path == path;

        let wildcard = if !matches && route.wildcard && path.starts_with(route.path) {
            matches = true;
            Some(path[route.path.len()..].to_string())
        } else {
            None
        };

        if matches {
            return dispatch_handler(route.handler.as_ref(), wildcard, &body, &shared)
        }
    }
    
    warn!("No route for path");
    Box::new(future::ok(not_found_handler()))
}

fn dispatch_handler<S>(
    handler: &Handler<S>, wildcard: Option<String>, body: &Chunk, shared: &S,
) -> Box<HyperFuture> {
    let body_string = String::from_utf8(body.to_vec()).unwrap();
    let request = Request {
        body: body_string,
        wildcard,
    };

    // Run the actual handler
    let result = handler.handle(shared, request);

    // Check if the handler succeeded, if so just return the data, if not return an error
    let response = match result {
        Ok(response) => {
            info!(
                "{} Sending {} {}",
                Paint::masked("✔️ "),
                Paint::white("Success").bold(),
                Paint::blue("response"),
            );

            let mut builder = hyper::Response::builder();
            
            if let Some(content_type) = response.content_type {
                builder.header(hyper::header::CONTENT_TYPE, content_type.to_string());
            }
                
            builder.body(Body::from(response.body)).unwrap()
        },
        Err(error) => {
            match error {
                Error::NotFound => {
                    warn!("Handler Returned Not Found");
                    not_found_handler()
                },
                Error::InternalError(error) => {
                    error!("Handler Failed: {}", error);
                    internal_error_handler()
                }
            }
        },
    };

    Box::new(future::ok(response))
}

fn not_found_handler() -> hyper::Response<Body> {
    warn!(
        "{} Sending {} {}",
        Paint::masked("❌ "),
        Paint::white("Not Found").bold(),
        Paint::yellow("response"),
    );

    hyper::Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body("<h1>404 Not Found</h1>".into())
        .unwrap()
}


fn internal_error_handler() -> hyper::Response<Body> {
    error!(
        "{} Sending {} {}",
        Paint::masked("❌ "),
        Paint::white("Internal Server Error").bold(),
        Paint::red("response"),
    );

    hyper::Response::builder()
        .status(StatusCode::INTERNAL_SERVER_ERROR)
        .body("<h1>500 Internal Server Error</h1>".into())
        .unwrap()
}

impl<S> IntoFuture for ApiService<S> {
    type Future = future::FutureResult<Self::Item, Self::Error>;
    type Item = Self;
    type Error = void::Void;

    fn into_future(self) -> Self::Future {
        future::ok(self)
    }
}
