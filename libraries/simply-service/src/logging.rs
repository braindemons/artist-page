use {
    std::fmt::{Write},

    log::{Level},
    yansi::{Paint},
};

pub fn setup_logging() {
    fern::Dispatch::new()
        .format(move |out, message, record| {
            match record.level() {
                Level::Info =>
                    out.finish(format_args!("{}", Paint::blue(message))),
                Level::Trace =>
                    out.finish(format_args!("{}", Paint::purple(message))),
                Level::Error =>
                    out.finish(format_args!(
                        "{} [{}]",
                        Paint::red(message),
                        Paint::red("Error").bold(),
                    )),
                Level::Warn =>
                    out.finish(format_args!(
                        "{} [{}]",
                        Paint::yellow(message),
                        Paint::yellow("Warning").bold(),
                    )),
                Level::Debug => {
                    let mut file_line = String::new();
                    if let Some(file) = record.file() {
                        write!(file_line, "{}", Paint::blue(file));
                    }
                    if let Some(line) = record.line() {
                        write!(file_line, ":{}", Paint::blue(line));
                    }

                    out.finish(format_args!(
                        "{} {}\n{}",
                        Paint::blue("-->").bold(),
                        file_line,
                        record.args(),
                    ));
                }
            }
        })
        .level(log::LevelFilter::Debug)
        .level_for("hyper", log::LevelFilter::Info)
        .level_for("tokio_reactor", log::LevelFilter::Info)
        .chain(std::io::stdout())
        .apply()
        .unwrap();
}
