use {
    std::result,
    
    serde::{Serialize, Deserialize},
    mime::{Mime},
};

pub struct Request {
    pub body: String,
    pub wildcard: Option<String>,
}

impl Request {
    pub fn deserialize_body<'de, T: Deserialize<'de>>(&'de self) -> result::Result<T, String> {
        serde_json::from_str(&self.body)
            .map_err(|e| format!("{}", e))
    }
}

pub struct Response {
    pub body: Vec<u8>,
    pub content_type: Option<Mime>,
}

impl Response {
    pub fn serialize_body<T: Serialize>(body: T) -> result::Result<Self, String> {
        let body = serde_json::to_string(&body)
            .map_err(|e| format!("{}", e))?;
        
        Ok(Self {
            body: body.into(),
            content_type: Some(mime::APPLICATION_JSON),
        })
    }
}

/// Logs the error and relays the request to an error handler.
#[derive(Debug)]
pub enum Error {
    NotFound,
    InternalError(String),
}

impl From<String> for Error {
    fn from(error: String) -> Self {
        Error::InternalError(error)
    }
}

pub type Result = result::Result<Response, Error>;