use {
    std::collections::{HashMap},

    serde_json::{Value, Map},
    marksman_escape::{Escape},

    simply_frontend::{
        template::{Element, Node, ElementName},
        App,
    },
};

pub fn render_html(app: &App) -> Result<String, String> {
    let mut html = String::new();

    let root_name = app.root_name();
    let bundle = app.initialize(root_name)?;
    let template = &app.class(root_name)?.template;
    let model = bundle.data.model_to_object()?;

    render_element(&mut html, template, &model, true, app)?;

    Ok(html)
}

fn render_element(
    html: &mut String, element: &Element, model: &Map<String, Value>, is_root: bool,
    app: &App,
) -> Result<(), String> {
    match &element.name {
        ElementName::Tag(tag) =>
            render_html_element(html, element, &tag, model, is_root, app),
        ElementName::Component(name) => {
            let bundle = app.initialize(&name)?;
            let template = &app.class(&name)?.template;
            let model = bundle.data.model_to_object()?;

            render_element(html, template, &model, false, app)
        },
        ElementName::ComponentBinding(binding) => {
            let name = binding.resolve_string(model)?;

            let bundle = app.initialize(&name)?;
            let template = &app.class(&name)?.template;
            let model = bundle.data.model_to_object()?;

            render_element(html, template, &model, false, app)
        },
    }
}

fn render_html_element(
    html: &mut String, element: &Element, tag: &str, model: &Map<String, Value>, is_root: bool,
    app: &App,
) -> Result<(), String> {
    html.push('<');
    html.push_str(tag);

    let mut one_time: HashMap<_, _> = element.attributes.one_time.iter().cloned().collect();
    
    // Insert the "app" ID on the root component, so it can be mounted on at runtime
    if is_root {
        one_time.insert("id".to_string(), "app".to_string());
    }

    // Add all the dynamic classes to the existing class entry if we have any, and if not add a new
    // class entry
    if !element.attributes.to_view_classes.is_empty() {
        let class_attribute = one_time.entry("class".into()).or_default();

        for (class, binding) in &element.attributes.to_view_classes {
            if binding.resolve_bool(model)? {
                class_attribute.push(' ');
                class_attribute.push_str(class);
            }
        }
    }

    for (key, value) in &one_time {
        html.push(' ');
        let escaped_key: Vec<u8> = Escape::new(key.bytes()).collect();
        html.push_str(&String::from_utf8_lossy(&escaped_key));
        html.push_str("=\"");
        let escaped_value: Vec<u8> = Escape::new(value.bytes()).collect();
        html.push_str(&String::from_utf8_lossy(&escaped_value));
        html.push('"');
    }

    if let Some(nodes) = &element.nodes {
        html.push('>');

        for section in nodes {
            match section {
                Node::Element(element) => {
                    render_element(html, element, model, false, app)?;
                },
                Node::Binding(binding) => {
                    let text = binding.resolve_string(model)?;
                    
                    let escaped: Vec<u8> = Escape::new(text.bytes()).collect();
                    html.push_str(&String::from_utf8_lossy(&escaped));
                },
                Node::Text { text, escape } => {
                    if *escape {
                        let escaped: Vec<u8> = Escape::new(text.bytes()).collect();
                        html.push_str(&String::from_utf8_lossy(&escaped));
                    } else {
                        html.push_str(&text);
                    }
                },
            }
        }

        html.push_str("</");
        html.push_str(tag);
        html.push('>');
    } else {
        html.push_str(" />");
    }

    Ok(())
}
