use {
    log::{Log, Level, Metadata, Record, SetLoggerError},
    web_sys::{console},
};

struct WasmLogger {
    level: Level,
}

impl Log for WasmLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.level
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let text = format!(
                "{} [{}]",
                record.args(),
                record.module_path().unwrap_or_default(),
            );
            
            match record.level() {
                Level::Trace =>
                    console::log_1(&text.into()),
                Level::Debug =>
                    console::log_1(&text.into()),
                Level::Info =>
                    console::info_1(&text.into()),
                Level::Warn =>
                    console::warn_1(&text.into()),
                Level::Error =>
                    console::error_1(&text.into()),
            }
        }
    }

    fn flush(&self) {
    }
}

pub fn init_with_level(level: Level) -> Result<(), SetLoggerError> {
    let logger = WasmLogger { level };
    log::set_boxed_logger(Box::new(logger))?;
    log::set_max_level(level.to_level_filter());
    Ok(())
}

pub fn init() -> Result<(), SetLoggerError> {
    init_with_level(Level::Trace)
}