use {
    std::{
        collections::{HashMap, VecDeque},
    },

    wasm_bindgen::{prelude::*, JsCast},
    web_sys::{Element, Node, Document, EventTarget, HtmlInputElement, Event},
    log::{info},
    slotmap::{DefaultKey},

    simply_frontend::{
        template::{self, ElementName, ValueBinding},
    },

    crate::{dom_events, State, StateRef, View, THREAD_LOCAL, DynamicClass},
};

struct HydrateContext {
    document: Document,
    state_ref: StateRef,
    queued: VecDeque<QueuedComponent>,
}

struct QueuedComponent {
    name: String,
    target: Element,
    parent: Option<DefaultKey>,
    dynamic: Option<ValueBinding>,
}

/// Renders the initial HTML, with all static data included. Only dynamic elements are remembered.
pub fn hydrate(document: &Document, state_ref: &StateRef) -> Result<(), String> {
    info!("Hydrating DOM");

    let mut state = state_ref.borrow_mut();

    let root_name = state.app.root_name().to_string();
    let dom_root = hydrate_component_tree(
        document, &mut state, state_ref,
        root_name, None,
    )?;

    document.get_element_by_id("app").unwrap()
        .replace_with_with_node_1(dom_root.as_ref())
        .unwrap();

    Ok(())
}

// TODO: This family of functions needs renaming, perhaps "initialize" or "generate_dom"?
pub fn hydrate_component_tree(
    document: &Document, state: &mut State, state_ref: &StateRef,
    name: String, parent: Option<DefaultKey>,
) -> Result<Element, String> {
    // Add the root component to the queue
    let mut queued = VecDeque::new();
    queued.push_back(QueuedComponent {
        name,
        target: document.create_element("div").unwrap(),
        parent,
        dynamic: None,
    });
    
    let mut context = HydrateContext {
        document: document.clone(),
        state_ref: state_ref.clone(),
        queued,
    };

    let mut dom_root = None;
    while let Some(queued) = context.queued.pop_front() {
        let element = hydrate_component(state, &mut context, &queued)?;

        // Record the first generated DOM element
        if dom_root.is_none() {
            dom_root = Some(element);
        }
    }

    Ok(dom_root.unwrap())
}

fn hydrate_component(
    state: &mut State, context: &mut HydrateContext, queued: &QueuedComponent,
) -> Result<Element, String> {
    // Initialize the component itself
    let bundle = state.app.initialize(&queued.name)?;

    // Prepare a component context bundle, this has all the resources the hydration process
    // need and can record dynamic bindings into
    let template = &state.app.class(&queued.name)?.template;
    let model = bundle.data.model_to_object()?;
    let view_id = state.views.insert(View {
        data: bundle.data,
        children: Vec::new(),
        model,
        dynamic_texts: HashMap::new(),
        dynamic_classes: HashMap::new(),
        dynamic_components: HashMap::new(),
    });

    // Record this element as a child element if it has a parent
    if let Some(parent) = queued.parent {
        state.views.get_mut(parent).unwrap().children.push(view_id);
    }

    // Record the view as an event sink if the component tells us to
    if let Some(event_sink) = bundle.event_sink {
        state.global_event_sinks.insert(event_sink, view_id);
    }

    // Create our component's view DOM
    let view = state.views.get_mut(view_id).unwrap();
    let element = hydrate_element(template, context, view, view_id)?;

    // Insert the created view DOM into the parent DOM
    queued.target.replace_with_with_node_1(element.as_ref()).unwrap();

    // If this component was marked dynamic, add it to its parent
    if let Some(binding) = &queued.dynamic {
        let parent_view = state.views.get_mut(queued.parent.unwrap()).unwrap();
        parent_view.dynamic_components
            .entry(binding.0.clone())
            .or_default()
            .push(element.clone());
    }

    Ok(element)
}

fn hydrate_element(
    element: &template::Element,
    context: &mut HydrateContext, view: &mut View, view_id: DefaultKey,
) -> Result<Element, String> {
    match &element.name {
        ElementName::Tag(tag) => {
            hydrate_html_element(
                element, tag,
                context, view, view_id,
            )
        },
        ElementName::Component(name) => {
            let dom_element = context.document.create_element("div").unwrap();
            context.queued.push_back(QueuedComponent {
                name: name.clone(),
                target: dom_element.clone(),
                parent: Some(view_id),
                dynamic: None,
            });
            Ok(dom_element)
        },
        ElementName::ComponentBinding(binding) => {
            let name = binding.resolve_string(&view.model)?;

            let dom_element = context.document.create_element("div").unwrap();
            context.queued.push_back(QueuedComponent {
                name: name.clone(),
                target: dom_element.clone(),
                parent: Some(view_id),
                dynamic: Some(binding.clone()),
            });

            Ok(dom_element)
        }
    }
}

fn hydrate_html_element(
    element: &template::Element, tag: &str,
    context: &mut HydrateContext, view: &mut View, view_id: DefaultKey,
) -> Result<Element, String> {
    let dom_element = context.document.create_element(tag).unwrap();

    // Add all attributes
    for (key, value) in &element.attributes.one_time {
        dom_element.set_attribute(key, value).unwrap();
    }

    // Check classes with bindings on if we need to add them
    for (class, binding) in &element.attributes.to_view_classes {
        if binding.resolve_bool(&view.model)? {
            dom_element.class_list().add_1(class).unwrap();
        }

        // Remember this as a dynamic class so we can look for model changes
        view.dynamic_classes
            .entry(binding.0.clone())
            .or_default()
            .push(DynamicClass {
                target: dom_element.clone(),
                class: class.clone()
            });
    }
    
    // Add input bindings
    hydrate_input_bindings(
        &element.attributes.from_view,
        &context.state_ref, &dom_element, view_id,
    )?;

    // Add events for this element
    hydrate_events(
        &element.attributes.events,
        &context.state_ref, &dom_element,
        view, view_id,
    )?;

    // Add all children
    if let Some(nodes) = &element.nodes {
        hydrate_children(
            nodes, &dom_element,
            context, view, view_id,
        )?;
    }

    Ok(dom_element)
}

fn hydrate_input_bindings(
    from_view: &[(String, ValueBinding)],
    state_ref: &StateRef, dom_element: &Element, view_id: DefaultKey,
) -> Result<(), String> {
    for (attribute, binding) in from_view {
        if attribute != "value" {
            return Err("Only bindings on \"value\" are supported right now".to_string())?
        }

        // Register an event so that on any changes to input, the model gets updated
        let state_ref = state_ref.clone();
        let binding = binding.clone();
        let input_element = dom_element.dyn_ref::<HtmlInputElement>()
            .ok_or_else(|| "Element with binding is not an input element".to_string())?
            .clone();
        let closure = Closure::wrap(
            Box::new(move || dom_events::handle_input(
                &state_ref, view_id, &binding, &input_element,
            )) as Box<FnMut()>
        );
        
        let event_target: &EventTarget = dom_element.as_ref();
        event_target.add_event_listener_with_callback("input", closure.as_ref().unchecked_ref())
            .unwrap();

        // We need to store closures to keep the state reference and the closure itself active
        THREAD_LOCAL.with(move |v| {
            v.borrow_mut().view_closures
                .entry(view_id)
                .or_default()
                .input.push(closure);
        });
    }

    Ok(())
}

fn hydrate_events(
    events: &[(String, template::Event)],
    state_ref: &StateRef, dom_element: &Element,
    view: &View, view_id: DefaultKey,
) -> Result<(), String> {
    for (key, event) in events {
        // Bind to the click event
        let state_ref = state_ref.clone();
        let event = view.data.parse_event(event)?;
        let callback = move |dom_event: Event| {
            dom_events::handle_event(&dom_event, &state_ref, view_id, &event)
        };
        let closure = Closure::wrap(Box::new(callback) as Box<FnMut(Event)>);
        
        let event_target: &EventTarget = dom_element.as_ref();
        event_target.add_event_listener_with_callback(&key, closure.as_ref().unchecked_ref())
            .unwrap();

        // We need to store closures to keep the state reference and the closure itself active
        THREAD_LOCAL.with(move |v| {
            v.borrow_mut().view_closures
                .entry(view_id)
                .or_default()
                .event.push(closure);
        });
    }

    Ok(())
}

fn hydrate_children(
    nodes: &[template::Node], dom_element: &Element,
    context: &mut HydrateContext, view: &mut View, view_id: DefaultKey,
) -> Result<(), String> {
    let element_node: &Node = dom_element.as_ref();

    for node in nodes {
        match node {
            template::Node::Element(child) => {
                let dom_child = hydrate_element(child, context, view, view_id)?;
                element_node.append_child(dom_child.as_ref()).unwrap();
            },
            template::Node::Binding(binding) => {
                // We've got a dynamic node, we need to perform the initial render and then store
                // it as a dynamic element
                let text = binding.resolve_string(&view.model)?;

                let dom_child = context.document.create_text_node(&text);
                element_node.append_child(dom_child.as_ref()).unwrap();

                view.dynamic_texts
                    .entry(binding.0.clone())
                    .or_default()
                    .push(dom_child);
            }
            template::Node::Text { text, escape } => {
                if *escape {
                    let dom_child = context.document.create_text_node(&text);
                    element_node.append_child(dom_child.as_ref()).unwrap();
                } else {
                    dom_element.insert_adjacent_html("beforeend", &text).unwrap();
                }
            },
        }
    }

    Ok(())
}