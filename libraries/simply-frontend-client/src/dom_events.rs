use {
    std::{
        any::{Any},
        collections::{VecDeque},
    },

    web_sys::{HtmlInputElement, Event, Document},
    slotmap::{DefaultKey},

    simply_frontend::{
        template::{ValueBinding},
        GlobalEvents,
    },

    crate::{State, StateRef, update_for_model_changes},
};

pub fn handle_input(
    state_ref: &StateRef, view_id: DefaultKey, binding: &ValueBinding,
    input_element: &HtmlInputElement
) {
    let mut state = state_ref.borrow_mut();
    let view = state.views.get_mut(view_id).unwrap();

    // Get some global stuff from the JS global scope that we'll need
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();

    // Change the model
    let mut model = view.data.model_to_object().unwrap();
    let new_value = input_element.value();
    binding.resolve_replace_string(&mut model, new_value).unwrap();

    // Submit the model changes back to the instance
    view.data.object_to_model(model.clone()).unwrap();

    // Update for any changes just in case input bound values are directly bound in the view
    update_for_model_changes(&document, &mut state, state_ref, view_id, model);
}

pub fn handle_event(
    dom_event: &Event, state_ref: &StateRef, view_id: DefaultKey, event: &Box<Any>,
) {
    let mut state = state_ref.borrow_mut();
    
    // Get some global stuff from the JS global scope that we'll need
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();

    // Links and submit buttons we're bound to should not navigate
    dom_event.prevent_default();

    // Dispatch the initial event
    let mut events = VecDeque::new();
    dispatch_event(&document, &mut state, state_ref, view_id, event, &mut events);

    // Go through global events queued by that dispatch, and dispatch those as well
    while let Some((sink, event)) = events.pop_front() {
        let view_id = state.global_event_sinks[sink];
        dispatch_event(&document, &mut state, state_ref, view_id, &event, &mut events);
    }
}

fn dispatch_event(
    document: &Document,
    state: &mut State, state_ref: &StateRef, view_id: DefaultKey, event: &Box<Any>,
    events: &mut VecDeque<(&'static str, Box<Any>)>,
) {
    // Notify the view
    let mut global_events = GlobalEvents::new(events);
    let view = state.views.get_mut(view_id).unwrap();
    view.data.raise_event(event, &mut global_events).unwrap();

    // Update the view's DOM data
    let model = view.data.model_to_object().unwrap();
    update_for_model_changes(&document, state, state_ref, view_id, model);
}