mod dom_events;
mod hydrate;

use {
    std::{
        collections::{HashMap},
        cell::{RefCell},
        rc::{Rc},
    },

    wasm_bindgen::prelude::*,
    web_sys::{Text, CharacterData, Element, Event, Document},
    serde_json::{Map, Value},
    slotmap::{SlotMap, DefaultKey},

    simply_frontend::{
        template::{value_to_bool, value_to_string},
        App, ComponentData,
    },
};

thread_local! {
    static THREAD_LOCAL: RefCell<ThreadLocal> = RefCell::new(ThreadLocal::default());
}

#[derive(Default)]
struct ThreadLocal {
    view_closures: HashMap<DefaultKey, ViewClosures>,
}

#[derive(Default)]
struct ViewClosures {
    input: Vec<Closure<FnMut() + 'static>>,
    event: Vec<Closure<FnMut(Event) + 'static>>,
}

pub struct State {
    app: App,
    views: SlotMap<DefaultKey, View>,
    global_event_sinks: HashMap<&'static str, DefaultKey>,
}

type StateRef = Rc<RefCell<State>>;

struct View {
    data: Box<ComponentData>,
    children: Vec<DefaultKey>,
    model: Map<String, Value>,

    dynamic_texts: HashMap<String, Vec<Text>>,
    dynamic_classes: HashMap<String, Vec<DynamicClass>>,
    dynamic_components: HashMap<String, Vec<Element>>,
}

struct DynamicClass {
    target: Element,
    class: String,
}

pub fn mount(app: App) -> Result<(), String> {
    // Get some global stuff from the JS global scope that we'll need
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();

    let state = State {
        app,
        views: SlotMap::new(),
        global_event_sinks: HashMap::new(),
    };
    let state = Rc::new(RefCell::new(state));

    hydrate::hydrate(&document, &state)
}

fn update_for_model_changes(
    document: &Document, state: &mut State, state_ref: &StateRef,
    view_id: DefaultKey, model: Map<String, Value>,
) {
    // Just do a simple loop, we aren't supporting nested types yet anyways
    for (key, value) in &model {
        let view = state.views.get(view_id).unwrap();
        let last_value = view.model.get(key)
            .ok_or_else(|| "Changing models aren't supported".to_string()).unwrap();

        // If nothing changed we can just ignore this
        if value == last_value {
            continue
        }

        // Something did change, re-render anything that should change as a result
        if let Some(texts) = view.dynamic_texts.get(key) {
            for text in texts {
                let text_value = value_to_string(value);
                let data_node: &CharacterData = text.as_ref();
                data_node.set_data(&text_value);
            }
        }

        if let Some(dynamic_classes) = view.dynamic_classes.get(key) {
            for dynamic_class in dynamic_classes {
                let list = dynamic_class.target.class_list();

                // Since we already know the value changed we can just rely on what it is now
                if value_to_bool(value) {
                    list.add_1(&dynamic_class.class).unwrap();
                } else {
                    list.remove_1(&dynamic_class.class).unwrap();
                }
            }
        }

        if let Some(dynamic_components) = view.dynamic_components.get(key).cloned() {
            let component_name = value_to_string(value);
            for (i, dynamic_component) in dynamic_components.iter().enumerate() {
                // TODO: Remove the views and callbacks, and children that aren't relevant anymore
                // after this

                // Render the new component and replace the old DOM tree with it
                let element = hydrate::hydrate_component_tree(
                    &document, state, state_ref, component_name.clone(), Some(view_id),
                ).unwrap();
                dynamic_component.replace_with_with_node_1(element.as_ref()).unwrap();

                // Replace the stored element with the new one so it can be replaced again later
                let view = state.views.get_mut(view_id).unwrap();
                let dynamic_components = &mut view.dynamic_components.get_mut(key).unwrap();
                dynamic_components[i] = element;
            }
        }
    }

    // Store the new model
    let view = state.views.get_mut(view_id).unwrap();
    view.model = model;
}