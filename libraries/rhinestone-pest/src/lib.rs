use {
    pest_derive::{Parser},
};

#[derive(Parser)]
#[grammar = "rhinestone.pest"]
pub struct RhinestoneParser;
